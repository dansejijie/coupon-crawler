const axios = require('axios');
const moment = require('moment');
const BASE_URL = "http://app.quanlaoda.com";

const bossAxios = axios.create({
  baseURL: BASE_URL,
})

const FOOD = 7; //食物类目
const nut = 339; //坚果
const specitaly = 340; // 地方特产
const pastry = 341; // 糕点
const vegetarian = 342; //素食
const fruit = 532; // 水果
const food = 573; // 海鲜肉类蔬果

const DIGITAL = 3; // 数码
const kitchen = 420; // 厨房家电

const HOME= 6; // 居家
const clean = 370; // 清洁
const groceries = 371; // 杂货

const categories= [
  {fup: FOOD, fid: nut},
  {fup: FOOD, fid: specitaly},
  {fup: FOOD, fid: pastry},
  {fup: FOOD, fid: vegetarian},
  {fup: FOOD, fid: fruit},
  {fup: FOOD, fid: food},

  {fup: DIGITAL, fid: kitchen},

  {fup: HOME, fid: clean},
  {fup: HOME, fid: groceries},
]

const params = {
  fup: 7,
  sort: 'desc',
  order: 'id',
  m: 'goods',
  fid: 339,
  a: 'list',
  page: 1,
}
const MAX_PAGE = 1;
const filterKey = [];
async function getGoods() {
  let result = [];
  let beginTime = moment();
  let progress = -1;
  let size = categories.length;
  for (let category of categories) {
    progress ++;
    console.log('progress:', progress/size, 'collects:', result.length);
    let page = 1;
    let hasMore = true;
    while(hasMore) {
      try {
        let res = await bossAxios.get('/app.php', {
          params: {
            ...params,
            fup: category.fup,
            fid: category.fid,
            page,
          },
        });
        res = res.data;
        if(res.status == "success" && res.data.length > 0) {
          result = result.concat(res.data);
        } else {
          hasMore = false;
          console.log(res);
        }
      } catch (err) {
        console.error(err)
      }
      page ++;
      if(page > MAX_PAGE) {
        hasMore = false;
      }
    }
  }
  let endTime = moment();
  console.log(`size${result.length} time:${endTime.valueOf() - beginTime.valueOf()}`)
  return result;
}

function handleData(results) {
  for (let item of results) {
    item.juan_price = parseFloat(item.juan_price)
    item.price = parseFloat(item.price);
    item.power = 0;
  }
  return results;
}

function pricePicker(data) {
  const rate = data.juan_price / data.price;
  console.log(rate);
  if( rate > 0.5) {
    return true;
  }
  return false;
}

function keyPicker(data) {
  for( let key of filterKey) {
    if(data.title.indexOf(key) >= 0) {
      return false;
    }
  }
  return true;
} 

function pickFitData(results) {
  results = handleData(results);
  const fitResults = results.filter((item)=>{
    try {
      if(!pricePicker(item)) {
        return false;
      }
      if(!keyPicker(item)) {
        return false;
      }
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
    
  })
  return fitResults;
}


module.exports = function getData() {
  return getGoods().then(res=>{
    return pickFitData(res);
  }).then(res2=>{
    for (let info of res2) {
      console.log(`${info.title} ${info.yh_price}\n`)
    }
  });
}




